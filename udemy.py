# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from time import sleep
import wget
import asyncio
import aiohttp
from aiofile import AIOFile,Writer
import os

async def get_file_async(url, folder, file_name):
    """
    Python3.5+
    安装 aiohttp aiofile
    异步下载文件
    :param url:
    :param folder:
    :param file_name:
    :return:
    """
    path = folder + file_name
    headers = {'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 '
                          'Firefox/3.5.5'}
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.get(url,timeout=9999999999999999999999) as r:
            async with AIOFile(path, 'wb') as afp:
                writer = Writer(afp)
                # reader = Reader(afp, chunk_size=8)
                result = await r.read()
                print(result)
                await writer(result)
                await afp.fsync()


if __name__ == "__main__":
    options = Options()
    options.add_argument('--headless')


    mobileEmulation = {'deviceName': 'iPhone X'} ##配置浏览器操作模式
    options = webdriver.ChromeOptions()
    options.add_experimental_option('mobileEmulation', mobileEmulation)



    options = webdriver.ChromeOptions()
    # options.add_argument('--no-sandbox')

    options.add_argument("user-data-dir=/Users/jimmy/Library/Application Support/Google/Chrome")
    options.add_argument('--profile-directory=Default')
    # options.add_argument('--enable-sync')
    options.add_experimental_option("excludeSwitches", ['disable-sync',
    'disable-background-networking','disable-client-side-phishing-detection','disable-default-apps','disable-hang-monitor','disable-popup-blocking','disable-prompt-on-repost','enable-automation','enable-blink-features','enable-logging',
    'load-extension','log-level','no-first-run','password-store','remote-debugging-port','test-type','use-mock-keychain'
    ]);

    driver = webdriver.Chrome(executable_path='/Users/jimmy/Documents/myparser/chromedriver', chrome_options=options)

    driver.set_window_size(1024, 960)
    driver.get("https://www.udemy.com/course/advanced-rest-apis-flask-python/learn/lecture/12066734#overview")
    username = input("登入完 進入要爬的課程 按確認鍵")

    elements = driver.find_elements_by_class_name("section--section--BukKG")
    print(elements)
    element = elements[0]
    chapters = []
    for i,element in enumerate(elements):
        element.click()
        chapter = element.find_element_by_class_name("section--title--eCwjX")
        chapterTitle = chapter.find_element_by_tag_name("span").find_element_by_tag_name("span").text
        print(chapterTitle)

        colusorEle = element.find_elements_by_class_name("curriculum-item-link--item-container--1ptOz")
        colusors = []
        for i,colusor in enumerate(colusorEle):
            try:
                colusorTitle = colusor.find_element_by_tag_name("span").find_element_by_tag_name("span").text
                colusor.click()
                sleep(5)
                link = driver.find_element_by_class_name("vjs-tech").get_attribute('src')
                print(link)
                colusors.append({
                    "colusorTitle":colusorTitle,
                    "link":link
                })
            except Exception as err:
                print(err)

        chapters.append({"chapterTitle":chapterTitle,
        "colusors":colusors
        })
    print(chapters)
    input("最好複製一下videoLinks結果 預防萬一 按確認鍵繼續")
    bbb = []

    # for (i,ii) in [(6,33),(8,53),(10,62),(13,80),(13,82),(13,83)]:
    #     print(i,ii)
    #     colusor = []
    #     for xx in chapters[i-1]["colusors"]:
    #         if xx["colusorTitle"].split('.')[0] == str(ii):
    #             colusor = xx

    #     bbb.append(
    #         {"chapterTitle":chapters[i-1]["chapterTitle"],
    #         "colusors":[colusor]}
    #     )
    # chapters = bbb

    print(chapters)
    loop = asyncio.get_event_loop()
    tasks = []

    for chapter in chapters:
        chapterTitle = chapter["chapterTitle"]
        colusors = chapter["colusors"]
        path = "/Users/jimmy/Documents/Udemy/Flask/"+chapterTitle
        if not os.path.isdir(path):
            os.mkdir(path)
        print(colusors)
        for colusor in colusors:
            tasks.append(get_file_async(colusor["link"],path+"/",colusor['colusorTitle']+".mp4"))
        # tasks = [get_file_async(colusor["link"],path+"/",colusor['colusorTitle']+".mp4") for colusor in colusors]
    loop.run_until_complete(asyncio.wait(tasks))

    input("看資料夾確認下載完成 按確認鍵結束")

    loop.close()
