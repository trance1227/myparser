from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from time import sleep
import wget
import asyncio
import aiohttp
from aiofile import AIOFile,Writer


async def get_file_async(url, folder, file_name):
    """
    Python3.5+
    安装 aiohttp aiofile
    异步下载文件
    :param url:
    :param folder:
    :param file_name:
    :return:
    """
    path = folder + file_name
    headers = {'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 '
                          'Firefox/3.5.5'}
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.get(url,timeout=9999999999999999999999) as r:
            async with AIOFile(path, 'wb') as afp:
                writer = Writer(afp)
                # reader = Reader(afp, chunk_size=8)
                result = await r.read()
                await writer(result)
                await afp.fsync()


if __name__ == "__main__":
    # options = Options()
    # options.add_argument('--headless')


    # mobileEmulation = {'deviceName': 'iPhone X'} ##配置浏览器操作模式
    # options = webdriver.ChromeOptions()
    # options.add_experimental_option('mobileEmulation', mobileEmulation)



    options = webdriver.ChromeOptions()
    # options.add_argument('--no-sandbox')

    # options.add_argument("user-data-dir=/Users/jimmy/Library/Application Support/Google/Chrome")
    # options.add_argument('--profile-directory=Default')
    # options.add_argument('--enable-sync')
    options.add_experimental_option("excludeSwitches", ['disable-sync',
    'disable-background-networking','disable-client-side-phishing-detection','disable-default-apps','disable-hang-monitor','disable-popup-blocking','disable-prompt-on-repost','enable-automation','enable-blink-features','enable-logging',
    'load-extension','log-level','no-first-run','password-store','remote-debugging-port','test-type','use-mock-keychain'
    ]);

    driver = webdriver.Chrome(executable_path='/Users/jimmy/Documents/myparser/chromedriver', chrome_options=options)

    # driver.set_window_size(1024, 960)
    driver.get("https://hiskio.com/courses/244/lectures/12198")
    username = input("登入完 進入要爬的課程 按確認鍵")

    elements = driver.find_elements_by_class_name("sec-link")
    print(elements)
    links = []
    for i,element in enumerate(elements):
        try:
            link = element.get_attribute('href')
            name = str(i).zfill(2) + "-" + element.find_element_by_class_name("ln-sec-title").text
            print(name)
            print(link)
            links.append((link,name))
        except Exception as err:
            print(err)

    print(links)

    videoLinks = []
    for link,name in links:
        try:
            driver.get(link)
            sleep(5)
            print(link)
            videoTag = driver.find_element_by_tag_name("video")
            videoLink = videoTag.get_attribute("src")
            print(videoLink)
            videoLinks.append((videoLink,name +'.mp4'))
        except Exception as err:
            print(err)
    print(videoLinks)
    input("最好複製一下videoLinks結果 預防萬一 按確認鍵繼續")

    videoLinks = []

    loop = asyncio.get_event_loop()
    tasks = [get_file_async(videoLink,"/Users/jimmy/Hisko/Vue/",name) for (videoLink,name) in videoLinks]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()

    input("看資料夾確認下載完成 按確認鍵結束")

